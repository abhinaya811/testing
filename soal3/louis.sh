#!/bin/bash

read -p "Enter username: " username
read -p "Enter password: " -s password

# check if password meets the criteria
if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || "$password" =~ $username || "$password" =~ chicken || "$password" =~ ernie ]]; then
    echo -e "\nPassword does not meet the criteria"
    exit 1
fi

# check if the username already exists in the users file
if grep -q "^$username:" ./users/users.txt; then
    echo "REGISTER: ERROR User already exists" >> ./users/log.txt
    echo "User already exists"
    exit 1
fi

# add the username and password to the users file
echo "$username:$password" >> ./users/users.txt
echo "REGISTER: INFO User $username registered successfully" >> ./users/log.txt
echo "User registered successfully"
